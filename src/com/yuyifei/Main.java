package com.yuyifei;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;

import soot.PackManager;
import soot.Scene;
import soot.SceneTransformer;
import soot.SootClass;
import soot.Transform;

public class Main extends SceneTransformer
{
	private static String rootDir = "/home/yuyifei/Downloads/yuyifei/AAA";
	private static String inJars = rootDir + "/libs/framework.jar"; 
	private static String outJar = rootDir + "/outJar/instrumentedframework.jar";
	private static String libJars = rootDir + "/libs/bouncycastle.jar" + File.pathSeparator
								+ rootDir + "/libs/core.jar" + File.pathSeparator
								+ rootDir + "/libs/ext.jar" + File.pathSeparator
								+ rootDir + "/libs/junit.jar";
	
	
	private static List<SootClass> classes = new ArrayList<SootClass>();
	private static Map<String,List<String>> uninstrumentedClasses = new HashMap<String, List<String>>();
	private static final String dummyMainClassName = "com.yuyifei.DummyMain";
	
	
	private static Pattern excludePat = Pattern.compile(
			 "(java\\..*)|(dalvik\\..*)|(android\\.os\\.(Parcel|Parcel\\$.*))|(android\\.util\\.Slog)|(android\\.util\\.(Log|Log\\$.*))");


	@Override
	protected void internalTransform(String phaseName, Map options)
	{
//		MyInstrumentor ci = new MyInstrumentor();
		MyInstrumentor ci = new MyInstrumentor();
		
		ci.instrument(classes);
		
		Scene.v().getApplicationClasses().remove(Scene.v().getSootClass(dummyMainClassName));
	}
	
	public static void main (String[] args)
	{
		System.out.println("----->:" + inJars);
		System.out.println("----->" + libJars);
		
		Scene.v().setSootClassPath("./src" + File.pathSeparator + inJars + File.pathSeparator + libJars);
		
		loadClassesToInstrument();
		
		PackManager.v().getPack("wjtp").add(new Transform("wjtp.yuyifei", new Main()));
		
		StringBuilder builder = new StringBuilder();
		builder.append("-w -p cg off -keep-line-number -keep-bytecode-offset ");
		//builder.append("-dynamic-class ");
		//builder.append("com.yuyifei.DummyMain ");
		//builder.append("-soot-classpath ");
		//builder.append("/home/yuyifei/Downloads/yuyifei/AAA/bin" + File.pathSeparator + inJars + File.pathSeparator + libJars + " ");
		//builder.append("-dynamic-package ");
		//builder.append("acteve.symbolic.integer. ");
		//builder.append("-dynamic-package ");
		//builder.append("com.yuyifei. ");
		//builder.append("-src-prec ");
		//builder.append("c ");
		//builder.append("-pp ");
		builder.append("-outjar -d ");
		builder.append(outJar+" ");
		builder.append("-O ");
		builder.append("-validate ");
		builder.append(dummyMainClassName);
		
		String[] sootArgs = builder.toString().split(" ");
		soot.Main.main(sootArgs);
		
		System.out.println("MyAddUninstrClassesToJar");
		new MyAddUninstrClassesToJar(uninstrumentedClasses, outJar).apply();
	}
	
	private static void loadClassesToInstrument()
    {
		for (String pname : inJars.split(File.pathSeparator)) {
			if (pname.endsWith(".jar")) {
				System.out.println("pname "+pname);
				JarFile jar = null;
				try {
					jar = new JarFile(pname);
				} catch(IOException e) {
					throw new RuntimeException(e.getMessage() + " " + pname);
				}
				if (jar == null)
					continue;
				for (Enumeration<JarEntry> e = jar.entries(); e.hasMoreElements();) {
					JarEntry entry = e.nextElement();
					String name = entry.getName();
					//System.out.println("------>" + name);
					if (name.endsWith(".class")) {
						name = name.replace(".class", "").replace(File.separatorChar, '.');
						if(!toBeInstrumented(name)){
							System.out.println("Skipped instrumentation of class: " + name);
							
//							try {
//								Thread.sleep(3000);
//							} catch (InterruptedException e1) {
//								// TODO Auto-generated catch block
//								e1.printStackTrace();
//							}
							
							addUninstrumentedClass(pname, name);
							continue;
						}
						try{
							SootClass klass = Scene.v().loadClassAndSupport(name);
							classes.add(klass);
						}catch(RuntimeException ex) {
							System.out.println("Failed to load class: " + name);
							addUninstrumentedClass(pname, name);
							if (ex.getMessage().startsWith("couldn't find class:")) {
								System.out.println(ex.getMessage());
							}
							else
								throw ex;
						}
					}
				}
			}
		}
    }
	
	private static void addUninstrumentedClass(String jarName, String className) {
		List<String> cs = uninstrumentedClasses.get(jarName);
		if (cs == null) {
			cs = new ArrayList();
			uninstrumentedClasses.put(jarName, cs);
		}
		cs.add(className);
	}
	
	private static boolean toBeInstrumented(String className)
	{
		if (true/*includePat.matcher(className).matches()*/) {
			return excludePat.matcher(className).matches() ? false : true;
		}
		return false;
	}
}
