package com.yuyifei;

import java.util.Iterator;
import java.util.List;

import soot.Body;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Unit;
import soot.Value;
import soot.jimple.IdentityStmt;
import soot.jimple.Jimple;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.StringConstant;
import soot.util.Chain;

public class MyInstrumentor 
{
	static private final java.io.PrintWriter printer = new java.io.PrintWriter(System.out);
	
	public void instrument(List<SootClass> classes)
	{
		for (SootClass klass : classes)
		{
			klass.setApplicationClass();
			//System.out.println("----->" + klass.getName());
		}
		
		for (SootClass klass : classes)
		{
			List<SootMethod> origMethods = klass.getMethods();
			for (SootMethod m : origMethods)
			{
				if (!m.isConcrete())
					continue;
				
				if (m.getSignature().equals("<android.telephony.SmsManager: void sendTextMessage"
						+ "(java.lang.String,java.lang.String,java.lang.String,android.app.PendingIntent,"
						+ "android.app.PendingIntent)>"))
				{
					System.out.println("********************************");
					System.out.println(m.getSignature());
					
					//myInstrument(m, "-------> HA HA HA, I FIND IT, instrument success. new jar file haha");
				}
			}
		}
	}
	
	private void myInstrument(SootMethod method, String info)
	{
		System.out.println("I will instrument a LOD method");
		
		SootMethod sm = Scene.v().getMethod("<android.util.Log: int i(java.lang.String,java.lang.String)>");
		System.out.println("------->" + sm.getSignature());
		
		Value logType = StringConstant.v("YUYIFEI");
		Value logMessage = StringConstant.v(info);
		
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(), logType, logMessage);
		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
		Body body = method.retrieveActiveBody();
		Chain<Unit> units = body.getUnits().getNonPatchingChain();
		
		Iterator<Unit> is = body.getUnits().snapshotIterator();
		while (is.hasNext())
		{
			Unit u = is.next();
			
			if(u instanceof IdentityStmt)
			{
				System.out.println(".......... >" + u.toString());
				continue;
			}
			
			System.out.println("--------->" + u.toString());
			
			units.insertBefore(generated, u);
			
			break;
		}
		//units.add(generated);
		
		System.out.println("Instrument finished!");
		
		myDebug(method, true);
	}
	
    public static void myDebug(SootMethod m, boolean flag)
    {
		if (flag) {
			try{
				Printer.v().printTo(m.retrieveActiveBody(), printer);
			}catch(RuntimeException e) {
				//failed to validate I guess
				System.out.println("Failed to validate: " + m.getSignature());
				for (Unit u : m.retrieveActiveBody().getUnits()) {
					System.out.println("\t"+u);
				}
				printer.flush();
				throw e;
			}
			printer.flush();
		}
    }
}
