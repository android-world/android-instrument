#实现流程

## 1、从编译后的Android源码中拷贝framework.jar和一些必要的jar包到工程目录的libs文件下。

    cp out/target/common/obj/JAVA_LIBRARIES/bouncycastle_intermediates/classes.jar $ROOT_DIR/libs/bouncycastle.jar
    cp out/target/common/obj/JAVA_LIBRARIES/ext_intermediates/classes.jar $ROOT_DIR/libs/ext.jar
    cp out/target/common/obj/JAVA_LIBRARIES/core_intermediates/classes.jar $ROOT_DIR/libs/core.jar
    cp out/target/common/obj/JAVA_LIBRARIES/core-junit_intermediates/classes.jar $ROOT_DIR/libs/junit.jar
    cp out/target/common/obj/JAVA_LIBRARIES/framework_intermediates/classes.jar $ROOT_DIR/libs/framework.jar

Android源码编译完成后会在(Android-source)/out/target/product/generic/system目录下生成最终的framework.jar文件，该文件里面是一个dex文件，\
该dex文件则是由out/target/common/obj/JAVA_LIBRARIES/framework_intermediates/classes.jar通过dx工具生成的。由于目前该项目还不支持直接对\
framework.jar里面的dex文件进行插桩，所以我们考虑对out/target/common/obj/JAVA_LIBRARIES/framework_intermediates/classes.jar进行插桩,\
然后通过dx工具重新生成一个framework.jar文件。

## 2、提取framework.jar中的class，将能够插桩的类与不能进行插桩的类进行分类
   
   目前尝试发现，现有版本的soot并不能对Android framework中的所有类进行插桩。因此，我们对jar中能够插桩的与不能进行插桩的进行分离，分别存放。\
   
        Main.main -> Main.loadClassesToInstrument -> Main.addUninstrumentedClass
        
注：不能插桩的类参考：（1）acteve里里面 exclude的类。（2）https://mailman.cs.mcgill.ca/pipermail/soot-list/2014-September/007328.html

## 3、使用soot对可插桩的class文件进行插桩
   
   正确配置好soot的参数后，即可通过soot框架完成对framework的插桩，程序运行完后会在outJar目录下生成插桩完的instrumentedframework.jar文件。
   
## 4、利用新生成的instrumentedframework.jar文件生成system.img 文件
    
    该部分任务可运行run.xml实现，run.xml是ant的配置文件，使用ant -f run.xml即可运行（ant -f run.xml clean 清除上一次生成的文件）,\
       (1）对instrumentedframework.jar文件进行分离
      （2）instrumentedframework.jar文件被分离成framework.jar和frameworkextras.tmp.jar两个jar文件
      （3）利用dx工具将framework.jar打包成classes.dex文件
      （4）利用dx工具将frameworkextras.tmp.jar打包成frameworkextras.jar
      （5）利用unyaffs工具打开system.img文件
      （6）将解压后的system.img文件生成的system目录下的framework.jar与（3）中生成的classes.dex文件进行合并
      （7）将（4）中生成的frameworkextras.jar文件拷贝到system目录下
      （8）利用mkyaffs2image工具将system目录重新打包生成新的system.img文件
      
## 修改ramdisk.img文件

对Android系统进行插桩后，我们将原有的framework.jar变成了一个先的framework.jar和一个frameworkextras.jar，
为了让frameworkextras.jar能被Android系统识别，我们需要修改init.rc文件，将frameworkextras.jar文件的路径添加进去，而init.rc文件是
存放在ramdisk.img 中的，因此，我们修改对ramdisk.img文件进行修改。

       rm -rf ramdisk
       mkdir ramdisk
       cd ramdisk
       cp $SDK_DIR/images/ramdisk.img .
       mv ramdisk.img ramdisk.cpio.gz
       gzip -d ramdisk.cpio.gz
       mkdir tmp
       cp ramdisk.cpio tmp
       cd tmp
       cpio -i -F ramdisk.cpio
       rm ramdisk.cpio
       cp $WORK_DIR/libs/init.rc .
       cpio -i -t -F ../ramdisk.cpio | cpio -o -H newc -O ../ramdisk_new.cpio
       cd ..
       gzip ramdisk_new.cpio
       mv ramdisk_new.cpio.gz ramdisk_new.img
       cp ramdisk_new.img $SDK_DIR/images/ramdisk.img

## 利用新生成的镜像文件启动emulator，则该模拟器即是修改后的模拟器

emulator是编译Android源码生成的一个工具，该工具能够启动Android模拟器。启动模拟器的命令为：

      $EMULATOR_DIR/emulator -system $SYSTEM_IMG/system.img -data $SDK_DIR/userdata.img -ramdisk $SDK_DIR/ramdisk.img